export const dataService = [
    {
        path: '/kelas-industri',
        picture: 'las la-industry',
        name: 'Kelas Industri',
    },
    {
        path: '/roadmap',
        picture: 'las la-tasks',
        name: 'Roadmap Keahlian',
    },
    {
        path: '/bootcamp',
        picture: 'las la-campground',
        name: 'Bootcamp',
    },
    {
        path: '/pelatihan',
        picture: 'las la-certificate',
        name: 'Pelatihan Bersertifikat',
    },
    {
        path: '/kurikulum',
        picture: 'las la-swatchbook',
        name: 'Sinkronisasi Kurikulum',
    },
    {
        path: '/uji-kompetensi',
        picture: 'las la-book-open',
        name: 'Uji Kompetensi',
    },
    {
        path: '/teaching',
        picture: 'las la-chalkboard-teacher',
        name: 'Teaching Factory',
    },
    {
        path: '/magang',
        picture: 'las la-building',
        name: 'PKL Bersertifikat',
    },
    {
        path: 'http://103.166.156.243/',
        isOtherWeb: true,
        picture: 'lab la-leanpub',
        name: 'E-Learning',
    },
]