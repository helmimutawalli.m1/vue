const DocMenuConfig = [
    {
      pages: [
        {
          heading: "Roadmap",
          route: "/roadmap",
          svgIcon: "media/icons/duotune/art/art002.svg",
          fontIcon: "bi-app-indicator",
        },
      ],
    },
  ];
  
  export default DocMenuConfig;
  